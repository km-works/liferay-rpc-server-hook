# README #

This is a mandatory component for liferay-portal-rpc. Since it is a Liferay SDK plugin project it has been setup as a separate repository for convenience. In order to compile this component you need to clone this repository into your Liferay SDK hook folder.