/*
 * Copyright (C) 2005-2015 Christian P. Lerch <christian.p.lerch [at] gmail [dot] com>
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this distribution. If not, see <http://www.gnu.org/licenses/>.
 */
package org.kmworks.liferay.rpc.server;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.MethodHandler;
import com.liferay.portal.kernel.util.MethodKey;
import com.liferay.portal.kernel.util.ObjectValuePair;
import com.liferay.portal.kernel.util.PortalClassLoaderUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.security.ac.AccessControlThreadLocal;
import com.liferay.portal.security.auth.HttpPrincipal;
import com.liferay.portal.security.auth.PrincipalThreadLocal;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.util.Encryptor;
import com.liferay.util.EncryptorException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.security.Key;
import javax.crypto.spec.SecretKeySpec;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.kmworks.liferay.rpc.utils.InputStreamSerializationProxy;
import org.kmworks.liferay.rpc.utils.Useful;

/**
 * @author Christian P. Lerch
 */
public class RPCTunnelServlet extends HttpServlet {

  private static final long serialVersionUID = 1L;
  private static final Log _log = LogFactoryUtil.getLog(RPCTunnelServlet.class);
  private static String sharedSecret;
  
  @Override
  public void doPost(HttpServletRequest request, HttpServletResponse response)
          throws IOException {

    ObjectInputStream ois;

    try {
      ois = new ObjectInputStream(request.getInputStream());
    } catch (IOException ioe) {
      if (_log.isWarnEnabled()) {
        _log.warn(ioe, ioe);
      }
      return;
    }

    Object returnObj = null;

    final boolean remoteAccess = AccessControlThreadLocal.isRemoteAccess();

    try {
      final Object obj = ois.readObject();

      @SuppressWarnings("unchecked")
      ObjectValuePair<HttpPrincipal, MethodHandler> ovp = (ObjectValuePair<HttpPrincipal, MethodHandler>) obj;

	    final HttpPrincipal principal = ovp.getKey();     
      MethodHandler methodHandler = ovp.getValue();
      
      if (methodHandler != null) {
        final MethodKey methodKey = methodHandler.getMethodKey();
        final Class<?> declaringClass = methodKey.getDeclaringClass();

        if (!isValidRequest(declaringClass) || !isValidSharedSecret(principal)) {
          return;
        }
        
        if (isLocalServiceRequest(declaringClass)) {
          AccessControlThreadLocal.setRemoteAccess(true);
        } else {
          // assert principal.getCompanyId() > 0
          final User currUser = UserLocalServiceUtil.getUserByScreenName(principal.getCompanyId(), principal.getLogin());
          // Set the userId for satisfying the authentication chain
          PrincipalThreadLocal.setName(currUser.getUserId());
          // Simulate a local call
          AccessControlThreadLocal.setRemoteAccess(false);
        }

        methodHandler = unproxyInputStreamArguments(methodHandler);

        returnObj = methodHandler.invoke(true);
      }
    } catch (InvocationTargetException ite) {
      
      returnObj = ite.getCause();

      if (!(returnObj instanceof PortalException)) {
        _log.error(ite, ite);

        if (returnObj != null) {
          Throwable throwable = (Throwable) returnObj;

          returnObj = new SystemException(throwable.getMessage());
        } else {
          returnObj = new SystemException();
        }
      }
    } catch (Exception e) {
      _log.error(e, e);
    } finally {
      AccessControlThreadLocal.setRemoteAccess(remoteAccess);
    }

    if (returnObj != null) {
      try {
        try (ObjectOutputStream oos = new ObjectOutputStream(response.getOutputStream())) {
          oos.writeObject(returnObj);
          oos.flush();
        }
      } catch (IOException ioe) {
        _log.error(ioe, ioe);
        throw ioe;
      }
    }
  }

  boolean isValidRequest(Class<?> clazz) {
    final String className = clazz.getName();
    return className.contains(".service.") && className.endsWith("ServiceUtil");
  }
  
  boolean isLocalServiceRequest(Class<?> clazz) {
    final String className = clazz.getName();
    return className.endsWith("LocalServiceUtil");
  }

  private MethodHandler unproxyInputStreamArguments(MethodHandler methodHandler) 
          throws FileNotFoundException {

    final Object[] args = methodHandler.getArguments();

    for (int i = 0; i < args.length; i++) {
      if (args[i] instanceof InputStreamSerializationProxy) {
        final InputStreamSerializationProxy proxy = (InputStreamSerializationProxy) args[i];
        final String fn = proxy.getBackingFileName();
        final File inp = new File(Useful.TEMP_DIR, fn);
        args[i] = new FileInputStream(inp);
      }
    }

    return new MethodHandler(methodHandler.getMethodKey(), args);
  }

  private static boolean isValidSharedSecret(HttpPrincipal principal) 
          throws ClassNotFoundException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException, 
          EncryptorException {
    if (sharedSecret == null) {
      Class<?> propsValues = PortalClassLoaderUtil.getClassLoader().loadClass("com.liferay.portal.util.PropsValues");
      Field sharedSecretFld = propsValues.getField("TUNNELING_SERVLET_SHARED_SECRET");
      sharedSecret = (String) sharedSecretFld.get(null);
    }
    if (sharedSecret != null) {
      Key key = new SecretKeySpec(sharedSecret.getBytes(), "AES");
      return principal.getLogin().equals(Encryptor.decrypt(key, principal.getPassword()));
    } else {
      return false;
    }
  }

}
