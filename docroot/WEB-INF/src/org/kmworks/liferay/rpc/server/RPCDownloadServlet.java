/*
 *  Copyright (C) 2005-2015 Christian P. Lerch <christian.p.lerch [at] gmail.com>
 * 
 *  This library is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU Lesser General Public License as published by the Free
 *  Software Foundation; either version 3 of the License, or (at your option)
 *  any later version.
 * 
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more 
 *  details.
 * 
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this distribution. If not, see <http://www.gnu.org/licenses/>.
 */
package org.kmworks.liferay.rpc.server;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.kmworks.liferay.rpc.utils.Useful;

/**
 *
 * @author Christian P. Lerch
 */
public class RPCDownloadServlet extends HttpServlet {

  private static final long serialVersionUID = 1L;
  private static final Log _log = LogFactoryUtil.getLog(RPCDownloadServlet.class);

  @Override
  public void doPost(HttpServletRequest request, HttpServletResponse response) {

    final String backingFileName;
    try (InputStream is = request.getInputStream()) {
      final byte[] buff = new byte[256];
      final int size = is.read(buff);
      backingFileName = new String(buff, 0, size);
    } catch (IOException ioe) {
      if (_log.isWarnEnabled()) {
        _log.warn(ioe, ioe);
      }
      return;
    }

    File downloadFile = new File(Useful.TEMP_DIR, backingFileName);

    final OutputStream ros;
    try (InputStream ris = new FileInputStream(downloadFile)) {
      ros = response.getOutputStream();
      final int bytesCopied = Useful.copyStream(ris, ros);
    } catch (IOException ioe) {
      if (_log.isWarnEnabled()) {
        _log.warn(ioe, ioe);
      }
    }
  }

}
