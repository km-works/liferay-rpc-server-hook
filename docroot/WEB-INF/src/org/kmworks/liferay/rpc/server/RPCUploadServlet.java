/*
 *  Copyright (C) 2005-2015 Christian P. Lerch <christian.p.lerch [at] gmail.com>
 * 
 *  This library is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU Lesser General Public License as published by the Free
 *  Software Foundation; either version 3 of the License, or (at your option)
 *  any later version.
 * 
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more 
 *  details.
 * 
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this distribution. If not, see <http://www.gnu.org/licenses/>.
 */
package org.kmworks.liferay.rpc.server;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.kmworks.liferay.rpc.utils.Useful;

/**
 *
 * @author Christian P. Lerch
 */
public class RPCUploadServlet extends HttpServlet {

  private static final long serialVersionUID = 1L;
  private static final Log _log = LogFactoryUtil.getLog(RPCUploadServlet.class);

  @Override
  public void doPost(HttpServletRequest request, HttpServletResponse response) {

    File tempFile;
    try (InputStream is = request.getInputStream()) {
      tempFile = File.createTempFile("RPC_", null);
      try (OutputStream fos = new FileOutputStream(tempFile)) {
        Useful.copyStream(is, fos);
      }
    } catch (IOException ioe) {
      if (_log.isWarnEnabled()) {
        _log.warn(ioe, ioe);
      }
      return;
    }

    try (OutputStream ros = response.getOutputStream()) {
      ros.write(tempFile.getName().getBytes("US-ASCII"));
    } catch (IOException ioe) {
      if (_log.isWarnEnabled()) {
        _log.warn(ioe, ioe);
      }
    }
  }

}
